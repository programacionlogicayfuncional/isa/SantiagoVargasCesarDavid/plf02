(ns plf02.core)


(defn función-associative?-1
  [a]
  (associative? a))

(defn función-associative?-2
  [a]
  (associative? a))

(defn función-associative?-3
  [a]
  (associative? a))

(función-associative?-1 1)
(función-associative?-2 "hola")
(función-associative?-3 :a)


(defn función-boolean?-1
  [a]
  (boolean? a))

(defn función-boolean?-2
  [a]
  (boolean? a))

(defn función-boolean?-3
  [a]
  (boolean? a))

(función-boolean?-1 true)
(función-boolean?-1 "hola")
(función-boolean?-1 0)


(defn función-char?-1
  [a]
  (char? a))

(defn función-char?-2
  [a]
  (char? a))

(defn función-char?-3
  [a]
  (char? a))

(función-char?-1 \b)
(función-char?-2 21)
(función-char?-3 \a)


(defn función-coll?-1
  [a]
  (coll? a))

(defn función-coll?-2
  [a]
  (coll? a))

(defn función-coll?-3
  [a]
  (coll? a))

(función-coll?-1 4)
(función-coll?-2 10)
(función-coll?-3 true)


(defn función-decimal?-1
  [a]
  (decimal? a))

(defn función-decimal?-2
  [a]
  (decimal? a))

(defn función-decimal?-3
  [a]
  (decimal? a))

(función-decimal?-1 "bueno")
(función-decimal?-2 2.0)
(función-decimal?-3 1M)


(defn función-double?-1
  [a]
  (double? a))

(defn función-double?-2
  [a]
  (double? a))

(defn función-double?-3
  [a]
  (double? a))

(función-double?-1 2.0)
(función-double?-2 "2")
(función-double?-3 \b)


(defn función-float?-1
  [a]
  (float? a))

(defn función-float?-2
  [a]
  (float? a))

(defn función-float?-3
  [a]
  (float? a))

(función-float?-1 0.0M)
(función-float?-2 0.0)
(función-float?-3 "hola")


(defn función-ident?-1
  [a]
  (ident? a))

(defn función-ident?-2
  [a]
  (ident? a))

(defn función-ident?-3
  [a]
  (ident? a))

(función-ident?-1 :hello)
(función-ident?-2 'abc)
(función-ident?-3 "hola")


(defn función-indexed?-1
  [a]
  (indexed? a))

(defn función-indexed?-2
  [a]
  (indexed? a))

(defn función-indexed?-3
  [a]
  (indexed? a))

(función-indexed?-1 2)
(función-indexed?-2 11)
(función-indexed?-3 "hola")


(defn función-int?-1
  [a]
  (int? a))

(defn función-int?-2
  [a]
  (int? a))

(defn función-int?-3
  [a]
  (int? a))

(función-int?-1 42)
(función-int?-2 10)
(función-int?-3 8)


(defn función-integer?-1
  [a]
  (integer? a))

(defn función-integer?-2
  [a]
  (integer? a))

(defn función-integer?-3
  [a]
  (integer? a))

(función-integer?-1 1)
(función-integer?-2 1.0)
(función-integer?-3 "bueno")


(defn función-keyword?-1
  [a]
  (keyword? a))

(defn función-keyword?-2
  [a]
  (keyword? a))

(defn función-keyword?-3
  [a]
  (keyword? a))

(función-keyword?-1 1)
(función-keyword?-2 :x)
(función-keyword?-3 true)


(defn función-list?-1
  [a]
  (list? a))

(defn función-list?-2
  [a]
  (list? a))

(defn función-list?-3
  [a]
  (list? a))

(función-list?-1 1)
(función-list?-2 2)
(función-list?-3 0)


(defn función-map-entry?-1
  [a]
  (map-entry? a))

(defn función-map-entry?-2
  [a]
  (map-entry? a))

(defn función-map-entry?-3
  [a]
  (map-entry? a))

(función-map-entry?-1 1)
(función-map-entry?-2 "hola")
(función-map-entry?-3 :v)


(defn función-map?-1
  [a]
  (map? a))

(defn función-map?-2
  [a]
  (map? a))

(defn función-map?-3
  [a]
  (map? a))

(función-map?-1 1)
(función-map?-2 "bye")
(función-map?-3 :a)

  
(defn función-nat-int?-1
  [a]
  (nat-int? a))

(defn función-nat-int?-2
  [a]
  (nat-int? a))

(defn función-nat-int?-3
  [a]
  (nat-int? a))

(función-nat-int?-1 0)
(función-nat-int?-2 2036857)
(función-nat-int?-3 -1)


(defn función-number?-1
  [a]
  (number? a))

(defn función-number?-2
  [a]
  (number? a))

(defn función-number?-3
  [a]
  (number? a))

(función-number?-1 2)
(función-number?-2 :a)
(función-number?-3 "23")


(defn función-pos-int?-1
  [a]
  (pos-int? a))

(defn función-pos-int?-2
  [a]
  (pos-int? a))

(defn función-pos-int?-3
  [a]
  (pos-int? a))

(función-pos-int?-1 9223372036854775807)
(función-pos-int?-2 "hola")
(función-pos-int?-3 :3)


(defn función-ratio?-1
  [a]
  (ratio? a))

(defn función-ratio?-2
  [a]
  (ratio? a))

(defn función-ratio?-3
  [a]
  (ratio? a))

(función-ratio?-1 22/7)
(función-ratio?-2 2)
(función-ratio?-3 :b)


(defn función-rational?-1
  [a]
  (rational? a))

(defn función-rational?-2
  [a]
  (rational? a))

(defn función-rational?-3
  [a]
  (rational? a))

(función-rational?-1 4)
(función-rational?-2 "hola")
(función-rational?-3 :a)


(defn función-seq?-1
  [a]
  (seq? a))

(defn función-seq?-2
  [a]
  (seq? a))

(defn función-seq?-3
  [a]
   (seq? a))

(función-seq?-1 4)
(función-seq?-2 "hola")
(función-seq?-3 \a)


(defn función-seqable?-1
  [a]
  (seqable? a))

(defn función-seqable?-2
  [a]
  (seqable? a))

(defn función-seqable?-3
  [a]
  (seqable? a))

(función-seqable?-1 nil)
(función-seqable?-2 3)
(función-seqable?-3 34/7)


(defn función-sequential?-1
  [a]
  (sequential? a))

(defn función-sequential?-2
  [a]
  (sequential? a))

(defn función-sequential?-3
  [a]
  (sequential? a))

(función-sequential?-1 4)
(función-sequential?-2 "adios")
(función-sequential?-3 :a)


(defn función-set?-1
  [a]
  (set? a))

(defn función-set?-2
  [a]
  (set? a))

(defn función-set?-3
  [a]
  (set? a))

(función-set?-1 6)
(función-set?-2 100)
(función-set?-3 \b)


(defn función-some?-1
  [a]
  (some? a))

(defn función-some?-2
  [a]
  (some? a))

(defn función-some?-3
  [a]
  (some? a))

(función-some?-1 8)
(función-some?-2 "bueno")
(función-some?-3 \a)


(defn función-string?-1
  [a]
  (string? a))

(defn función-string?-2
  [a]
  (string? a))

(defn función-string?-3
  [a]
  (string? a))

(función-string?-1 "hola")
(función-string?-2 7)
(función-string?-3 \b)


(defn función-symbol?-1
  [a]
  (symbol? a))

(defn función-symbol?-2
  [a]
  (symbol? a))

(defn función-symbol?-3
  [a]
  (symbol? a))

(función-symbol?-1 4)
(función-symbol?-2 "fin")
(función-symbol?-3 :b)


(defn función-vector?-1
  [a]
  (vector? a))

(defn función-vector?-2
  [a]
  (vector? a))

(defn función-vector?-3
  [a]
  (vector? a))

(función-vector?-1 5)
(función-vector?-2 6)
(función-vector?-3 7)




(defn función-drop-1
  [a b c d]
   (drop a [b c d]))

(defn función-drop-2
  [a b c d]
   (drop a [b c d]))

(defn función-drop-3
  [a b c d]
   (drop a [b c d]))

(función-drop-1 1 1 2 3)
(función-drop-2 6 5 6 7)
(función-drop-3 1 4 1 3)


(defn función-drop-last-1
  [a b]
  (drop-last [a b]))

(defn función-drop-last-2
  [a b]
  (drop-last [a b]))

(defn función-drop-last-3
  [a b]
  (drop-last [a b]))

(función-drop-last-1 3 6)
(función-drop-last-2 4 7)
(función-drop-last-3 5 10)


(defn función-drop-while-1
  [a b c d]
 (drop-while #(d a %) [b c]))

(defn función-drop-while-2
  [a b c d]
  (drop-while #(d a %) [b c]))

(defn función-drop-while-3
  [a b c d]
  (drop-while #(d a %) [b c]))

(función-drop-while-1 1 2 3 <)
(función-drop-while-2 2 3 4 >)
(función-drop-while-3 1 4 5 <=)


(defn función-every?-1
  [a b c d]
  (every? #{a b} [c d]))

(defn función-every?-2
  [a b c d]
  (every? #{a b} [c d]))

(defn función-every?-3
  [a b c d]
  (every? #{a b} [c d]))

(función-every?-1 1 2 3 4)
(función-every?-2 5 6 7 8)
(función-every?-3 9 10 11 12)


(defn función-filterv-1
  [a b c]
  (filterv c [a b]))

(defn función-filterv-2
  [a b c]
  (filterv c [a b]))

(defn función-filterv-3
  [a b c]
  (filterv c [a b]))

(función-filterv-1 5 9 <)
(función-filterv-2 8 6 >)
(función-filterv-3 4 7 <=)


(defn función-group-by-1
  [a b c]
  (group-by c [a b]))

(defn función-group-by-2
  [a b c]
  (group-by c [a b]))

(defn función-group-by-3
  [a b c]
  (group-by c [a b]))

(función-group-by-1 1 2 <)
(función-group-by-2 9 10 >)
(función-group-by-3 -1 100 >=)


(defn función-iterate-1
  [a b]
  (iterate a b))

(defn función-iterate-2
  [a b]
  (iterate a b))

(defn función-iterate-3
  [a b]
  (iterate a b))

(función-iterate-1 inc 5)
(función-iterate-2 dec 2)
(función-iterate-3 inc 10)


(defn función-keep-1
  [a b]
  (keep a b))

(defn función-keep-2
  [a b]
  (keep a b))

(defn función-keep-3
  [a b]
  (keep a b))

(función-keep-1 {:a 1} [:a :b])
(función-keep-2 {:a 10} [:b :a])
(función-keep-3 {:b 11} [:a :a])


(defn función-keep-indexed-1
  [a b]
 (keep-indexed a b))

(defn función-keep-indexed-2
  [a b]
  (keep-indexed a b))

(defn función-keep-indexed-3
  [a b]
  (keep-indexed a b))

(función-keep-indexed-1 #(if (pos? %2) %1) [1 3])
(función-keep-indexed-2 #(if (pos? %2) %1) [7 6])
(función-keep-indexed-3 #(if (pos? %2) %1) [9 0])


(defn función-map-indexed-1
  [a b]
  (map-indexed a b))

(defn función-map-indexed-2
  [a b]
  (map-indexed a b))

(defn función-map-indexed-3
  [a b]
  (map-indexed a b))

(función-map-indexed-1 vector "mira")
(función-map-indexed-2 vector "no se puede")
(función-map-indexed-3 vector "a mira no mas")


(defn función-mapcat-1
  [a b]
  (mapcat a b))

(defn función-mapcat-2
  [a b]
  (mapcat a b))

 (defn función-mapcat-3
   [a b]
   (mapcat a b))

(función-mapcat-1 reverse [[1] [3]])
(función-mapcat-2 reverse [[4] [5]])
(función-mapcat-3 reverse [[100] [10]])


(defn función-mapv-1
  [a b]
  (mapv a b))

(defn función-mapv-2
  [a b]
  (mapv a b))

(defn función-mapv-3
  [a b]
  (mapv a b))

(función-mapv-1 inc [5])
(función-mapv-2 dec [8])
(función-mapv-3 inc [9])


(defn función-merge-with-1
  [a b c]
  (merge-with c b a))

(defn función-merge-with-2
  [a b c]
  (merge-with c b a))

(defn función-merge-with-3
  [a b c]
  (merge-with c b a))

(función-merge-with-1 {:a 1} {:b 2} +)
(función-merge-with-2 {:a 4} {:b 3} +)
(función-merge-with-3 {:a 4} {:b 3} +)


(defn función-not-any?-1
  [a b]
  (not-any? a b))

(defn función-not-any?-2
  [a b]
  (not-any? a b))

(defn función-not-any?-3
  [a b]
  (not-any? a b))

(función-not-any?-1 nil? [true false false])
(función-not-any?-2 nil? [true false false])
(función-not-any?-3 nil? [true false false])


(defn función-not-every?-1
  [a b]
  (not-every? a b))

(defn función-not-every?-2
  [a b]
  (not-every? a b))

(defn función-not-every?-3
  [a b]
  (not-every? a b))

(función-not-every?-1 odd? (vector 1 2))
(función-not-every?-2 odd? (vector 11 22))
(función-not-every?-3 odd? (vector 0 25))


(defn función-partition-by-1
  [a b]
  (partition-by a b))

(defn función-partition-by-2
  [a b]
  (partition-by a b))

(defn función-partition-by-3
  [a b]
  (partition-by a b))

(función-partition-by-1 #(= 3 %) [5])
(función-partition-by-2 #(= 1 %) [4])
(función-partition-by-3 #(= 2 %) [10])


(defn función-reduce-kv-1
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-2
  [a b c]
  (reduce-kv a b c))
  
(defn función-reduce-kv-3
  [a b c]
  (reduce-kv a b c))
  
(función-reduce-kv-1 #(assoc %1 %3 %2) {} {:a 1})
(función-reduce-kv-2 #(assoc %1 %2 %3) {} {:a 10})
(función-reduce-kv-3 #(assoc %1 %3 %2) {} {:a 2})


(defn función-remove-1
  [a b]
  (remove a b))

(defn función-remove-2
  [a b]
  (remove a b))

(defn función-remove-3
  [a b]
  (remove a b))

(función-remove-1 pos? [1 2])
(función-remove-2 pos? [4 3])
(función-remove-3 pos? [7 9])


(defn función-reverse-1
  [a]
  (reverse a))

(defn función-reverse-2
  [a]
  (reverse a))

(defn función-reverse-3
  [a]
  (reverse a))

(función-reverse-1 (vector 1 2))
(función-reverse-2 (vector 10 9))
(función-reverse-3 (vector 1 20))


(defn función-some-1
  [a b]
  (some a b))

(defn función-some-2
  [a b]
  (some a b))

(defn función-some-3
  [a b]
  (some a b))

(función-some-1 even? (vector 1 2))
(función-some-2 even? (vector 10))
(función-some-3 even? (vector 10 9))


(defn función-sort-by-1
  [a b]
  (sort-by a b))

(defn función-sort-by-2
  [a b]
  (sort-by a b))

(defn función-sort-by-3
  [a b]
  (sort-by a b))

(función-sort-by-1 first [[2 5] [1 3]])
(función-sort-by-2 first [[2 10] [9 3]])
(función-sort-by-3 first [[9 5] [5 4]])

  
(defn función-split-with-1
  [a b]
  (split-with a b))

(defn función-split-with-2
  [a b]
  (split-with a b))

(defn función-split-with-3
  [a b]
  (split-with a b))

(función-split-with-1 (partial >= 3) [1 2])
(función-split-with-2 (partial >= 3) [3 4])
(función-split-with-3 (partial >= 3) [5 6])


(defn función-take-1
  [a]
  (take 3 a))

(defn función-take-2
  [a]
  (take 3 a))

(defn función-take-3
  [a]
  (take 3 a))

(función-take-1 [1 2 3])
(función-take-2 [1 8 9])
(función-take-3 [4 0 3])


(defn función-take-last-1
  [a b]
  (take-last a b))

(defn función-take-last-2
  [a b]
  (take-last a b))

(defn función-take-last-3
  [a b]
  (take-last a b))

(función-take-last-1 2 [3 5])
(función-take-last-2 5 [7 8 7])
(función-take-last-3 1 [0 9 3 4])


(defn función-take-nth-1
  [a b]
  (take-nth a b))

(defn función-take-nth-2
  [a b]
  (take-nth a b))

(defn función-take-nth-3
  [a b]
  (take-nth a b))

(función-take-nth-1 2 (range 6))
(función-take-nth-2 20 (range 45))
(función-take-nth-3 5 (range 15))


(defn función-take-while-1
  [a b]
  (take-while a b))

(defn función-take-while-2
  [a b]
  (take-while a b))

(defn función-take-while-3
  [a b]
  (take-while a b))

(función-take-while-1 (partial > 1) (iterate inc 10))
(función-take-while-2 (partial > 2) (iterate inc 20))
(función-take-while-3 (partial > 3) (iterate inc 30))


(defn función-update-1
  [a b c]
  (update a b c))

(defn función-update-2
  [a b c]
  (update a b c))

(defn función-update-3
  [a b c]
  (update a b c))

(función-update-1 [1 2 3] 0 inc)
(función-update-2 [1 2 3] 1 dec)
(función-update-3 [4 4 38] 2 inc)


(defn función-update-in-1
  [a b c d e]
  (update-in a b c d e))

(defn función-update-in-2
  [a b c d e]
  (update-in a b c d e))

(defn función-update-in-3
  [a b c d e]
  (update-in a b c d e))

(función-update-in-1 {:a 7} [:a] / 4 5)
(función-update-in-2 {:b 6} [:b] / 3 9)
(función-update-in-3 {:c 5} [:c] / 1 15)
